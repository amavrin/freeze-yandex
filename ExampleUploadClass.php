<?php

/*
 * someTrait
 */
trait someTrait {

    /**
     * @param $s
     */
	public function doStuff($s)
	{
		return $s + 1;
	}
}

/*
 * BasketSupport global class
 */
class BasketSupport {

	use someTrait;

    protected function __clone() {

    }

    private function __construct() {

    }

    /**
     * @param $array
     * @param $path
     * @param $value
     * @return array
     */
    public static function updateIntoArray($array, $path, $value): array
    {
        if(!is_array($array)){
            return false;
        }
        if(!is_array($path)){
            $path = array($path);
        }

        $work_array = &$array;
        foreach($path as $one_path){
            if(!array_key_exists($one_path, $work_array)){
                $work_array[$one_path] = NULL;
            }

            $work_array = &$work_array[$one_path];
        }
        if(is_array($work_array) && is_array($value)){
            $work_array = array_merge($work_array,$value);
        }elseif(is_array($work_array) && !is_array($value)){
            $work_array = array_merge($work_array,array($value));
        }elseif(!is_array($work_array) && is_array($value)){
            $work_array = array_merge(array($work_array),$value);
        }else{
            $work_array = $value;
        }
        return $array;
    }

    /**
     * @param $array
     * @param $path
     * @return array
     */
    public static function getFromArray($array, $path): array
    {
        if(!is_array($array)){
            return false;
        }
        if(!is_array($path)){
            $path = array($path);
        }

        $work_array = &$array;
        foreach($path as $one_path){
            if(!array_key_exists($one_path, $work_array)){
                $work_array[$one_path] = NULL;
            }

            $work_array = &$work_array[$one_path];
        }
        return $work_array;
    }

    /**
     * @param $IDs
     * @param $arInitProps
     * @param bool $arOfferProps
     * @return array
     */
    public static function getPropertyForBasketByList($IDs, $arInitProps, $arOfferProps = false): array
    {

        $dependence = array();

        if(count($IDs)>0){
            CModule::IncludeModule('iblock');
            CModule::IncludeModule('catalog');

            $arSelect = array("ID","IBLOCK_ID","CODE","NAME");
            foreach($arInitProps as $prop){
                $arSelect[] = 'PROPERTY_'.$prop;
            }

            if($arOfferProps){
                foreach($arOfferProps as $prop){
                    $arSelect[] = 'PROPERTY_'.$prop;
                }
            }

            $productToOffers = array();
            $productIDs = array();

            $res_elem = CIBlockElement::GetList(
                array(),
                array("ID"=>$IDs),
                false,
                false,
                $arSelect);
            if($res_elem->SelectedRowsCount()>0){
                while($ar_elem = $res_elem->GetNextElement()){
                    $arFields = $ar_elem->GetFields();
                    $arProps = $ar_elem->GetProperties();

                    $mxResult = CCatalogSKU::GetInfoByProductIBlock($arFields['IBLOCK_ID']);

                    if($mxResult){
                        // products 
                        foreach($arProps as $prop_key=>$ar_prop){
                            if(in_array($ar_prop['CODE'],$arInitProps)){
                                $dependence[$arFields['ID']]["PROPS"][$prop_key] = $ar_prop;
                            }
                        }

                        $productToOffers[$arFields['ID']][] = $arFields['ID'];
                    }else{
                        // sku

                        //pre('SKU');
                        if($arOfferProps){
                            foreach($arProps as $prop_key=>$ar_prop){
                                if(in_array($ar_prop['CODE'],$arOfferProps)){
                                    $dependence[$arFields['ID']]["PROPS"][$prop_key] = $ar_prop;
                                }
                            }
                        }

                        $mxRes = CCatalogSku::GetProductInfo($arFields['ID']);

                        if($mxRes){
                            //pre($mxRes);
                            $productToOffers[$mxRes['ID']][] = $arFields['ID'];
                            $productIDs[]  = $mxRes['ID'];
                        }
                    }
                }





                if(count($productIDs)>0){
                    $res_elem = CIBlockElement::GetList(
                        array(),
                        array(
                            "ID"=>$productIDs,
                        ),
                        false,
                        false,
                        $arSelect);
                    while($ar_elem = $res_elem->GetNextElement()){
                        $arProps = $ar_elem->GetProperties();
                        $arFields = $ar_elem->GetFields();

                        foreach($productToOffers[$arFields['ID']] as $offer){
                            foreach($arProps as $prop_key=>$ar_prop){
                                if(in_array($ar_prop['CODE'],$arInitProps)){
                                    $dependence[$offer]["PROPS"][$prop_key] = $ar_prop;
                                }
                            }
                        }
                    }

                }


            }
        }

        return $dependence;
    }

}

