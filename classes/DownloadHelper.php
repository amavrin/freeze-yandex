<?php

/**
 * Class DownloadHelper
 */
class DownloadHelper {

    /**
     * @var InterfaceGenerator
     */
    private $generator;

    /**
     * InterfaceGenerator constructor.
     * @param $filePath
     */
    public function __construct(InterfaceGenerator $interfaceGenerator)
    {
        $this->setGenerator($interfaceGenerator);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return str_replace($_SERVER['DOCUMENT_ROOT'], "", $this->generator->getInterfaceFilePath());
    }

    /**
     * @param InterfaceGenerator $generator
     */
    public function setGenerator(InterfaceGenerator $generator): void
    {
        $this->generator = $generator;
    }


}
