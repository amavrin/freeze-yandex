<?php
use Funkyproject\ReflectionFile;
use Zend\Code\Reflection\ClassReflection;
use com\github\gooh\InterfaceDistiller\InterfaceDistiller;
use com\github\gooh\InterfaceDistiller\Filters\RegexMethodIterator;
use com\github\gooh\InterfaceDistiller\Filters\NoImplementedMethodsIterator;
use com\github\gooh\InterfaceDistiller\Filters\NoInheritedMethodsIterator;
use com\github\gooh\InterfaceDistiller\Filters\NoOldStyleConstructorIterator;
use com\github\gooh\InterfaceDistiller\Filters\NoMagicMethodsIterator;
use com\github\gooh\InterfaceDistiller\Distillate;

/**
 * Class InterfaceGenerator
 */
class InterfaceGenerator {

    /**
     * @var string
     */
    private $filePath;
    /**
     * @var string
     */
    private $saveFolderPath;
    /**
     * @var string
     */
    private $interfaceName;
    /**
     * @var string;
     */
    private $intefaceFileName;
    /**
     * @var string
     */
    private $interfaceFilePath;
    /**
     * @var ReflectionFile;
     */
    private $reflector;


    /**
     * InterfaceGenerator constructor.
     * @param $filePath
     */
    public function __construct($filePath, $saveFolderPath)
    {
        $this->setFilePath($filePath);
        $this->setSaveFolderPath($saveFolderPath);
        $this->includeFileClass();
        $this->setReflector();
        $this->setInterfaceName($this->reflector->getName());
        $this->setIntefaceFileName($this->interfaceName.'.txt');
        $this->setInterfaceFilePath($saveFolderPath);
    }

    /**
     *
     */
    public function includeFileClass()
    {	
		require_once($this->filePath);
    }

    /**
     * @return string
     */
    public function generate()
    {
            $methodIterator = new NoImplementedMethodsIterator(
                new NoInheritedMethodsIterator(
                    new NoOldStyleConstructorIterator(
                        new NoMagicMethodsIterator(
                            new \ArrayIterator($this->reflector->getMethods())
                        )
                    ),
                    $this->reflector
                )
            );

            $distillate = new Distillate;
            $distillate->setInterfaceName($this->interfaceName);
            foreach ($methodIterator as $method) {
                $distillate->addMethod($method);
            }

            $file = new \SplFileObject($this->interfaceFilePath, 'w');
            $writer = new Distillate\Writer($file);
            $writer->writeToFile($distillate);
            $file->rewind();
            $file->fpassthru();

            return $this->interfaceFilePath;

    }


    /**
     *
     */
    private function setReflector(): void
    {
        try {
            $reflectionFile = new ReflectionFile($this->filePath);

            $this->reflector = new \ReflectionClass($reflectionFile->getName());


        } catch (ReflectionException $exception) {

        }

    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     */
    private function setFilePath(string $filePath)
    {
        $this->filePath = $filePath;
    }


    /**
     * @return string
     */
    public function getSaveFolderPath(): string
    {
        return $this->saveFolderPath;
    }

    /**
     * @param string $saveFolderPath
     */
    private function setSaveFolderPath(string $saveFolderPath)
    {
        $this->saveFolderPath = $saveFolderPath;
    }

    /**
     * @return string
     */
    public function getInterfaceName(): ? string
    {
        return $this->interfaceName;
    }

    /**
     * @param string $interfaceName
     */
    private function setInterfaceName(string $interfaceName)
    {
        $this->interfaceName = $interfaceName.'Interface';
    }

    /**
     * @return string
     */
    public function getIntefaceFileName(): string
    {
        return $this->intefaceFileName;
    }

    /**
     * @param string $intefaceFileName
     */
    private function setIntefaceFileName(string $intefaceFileName): void
    {
        $this->intefaceFileName = $intefaceFileName;
    }

    /**
     * @return mixed
     */
    public function getInterfaceFilePath()
    {
        return $this->interfaceFilePath;
    }

    /**
     * @param mixed $saveFolderPath
     */
    private function setInterfaceFilePath($saveFolderPath): void
    {
        $this->interfaceFilePath = $saveFolderPath.'/'.$this->intefaceFileName;
    }

}
