$(function(){
    $('#upload').on('submit', function(e){
        e.preventDefault();
        let $that = $(this),
            formData = new FormData($that.get(0));
        $.ajax({
            url: $that.attr('action'),
            type: $that.attr('method'),
            contentType: false,
            processData: false,
            data: formData,
            //dataType: 'json',
            success: function(response){
                let uploadInfo = $('.upload_info');
                let linkWrap = $('.download_wrap');
                let link = $('.download_link');

                if (response.success) {
                    uploadInfo.html('');
                    linkWrap.show();
                    link.attr('href', response.success);
                }
                if (response.error) {
                    uploadInfo.append(response.error);
                    linkWrap.hide();
                    link.attr('href', '');
                }
				$that.append(response);
            }
        });
    });
});