<?php
    require_once(__DIR__."/../main/loader.php");
    header('Content-type: application/json');

    $response = [];

    //Определяем место сохранения загруженного файла и его имя
    $uploadFolder = __DIR__. '/upload';
    $downloadFolder = __DIR__.'/download';
    if (!file_exists($uploadFolder)) {
        mkdir($uploadFolder, 0700);
    }
    if (!file_exists($downloadFolder)) {
        mkdir($downloadFolder, 0700);
    }

    $fileTempName = $_FILES['class']['tmp_name'];
    $fileRealName = $_FILES['class']['name'];

    if (is_uploaded_file($fileTempName)) {

        $newFilePath = $uploadFolder .'/'.$fileRealName;

        //Перемещаем файл из временной папки в указанную
        if (move_uploaded_file($fileTempName, $newFilePath)) {

            $generator = new InterfaceGenerator($newFilePath, $downloadFolder);
            $generator->generate();
            $url = new DownloadHelper($generator);
            $response['success'] = $url->getUrl();

        } else {
            $response['error'] = 'Не удалось осуществить сохранение загруженного файла класса';
        }
    } else {
        $response['error'] = 'Файл класса не был загружен на сервер';
    }

    echo json_encode($response);

?>