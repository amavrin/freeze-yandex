<?php require_once(__DIR__."/../main/loader.php");  ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Тест</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        .form-control {
            line-height: 1em;
        }
    </style>
</head>

<body>

<div class="container">
    <h1 style="margin:50px 0;">Генератор интерфейса</h1>

    <form id="upload" action="file-loader.php" method="post" enctype="multipart/form-data">
        <div class="form-row">
            <div class="col">
                <input type="file" name="class" class="form-control" autofocus required title="Выберите файл/архив" placeholder="Выберите файл/архив">
            </div>
            <div class="col">
                <input type="submit" id="uploadButton" class="btn btn-primary" value="Сгенерировать">
            </div>
        </div>
    </form>
    <div class="upload_info"></div>
    <div class="download_wrap" style="display:none"><a href="#" target="_blank" class="download_link">Скачать</a></div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/index.js"></script>
</body>

</html>